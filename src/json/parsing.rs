use super::literals::*;
use super::{Object, ParsingError, ContinuousError};

pub enum Value {
    Obj(Object),
    Array(Vec<Value>),
    Number(f64),
    String(String),
    True,
    False,
    Null,
}

enum Next<'a> {
    NewPair(&'a str),
    Over(&'a str),
}

fn advance_until(content: &str, limit: char) -> Result<&str, ParsingError> {
    for (i, c) in content.chars().enumerate() {
        match c {
            CHARACTER_TABULATION | LINE_FEED | CARRIAGE_RETURN | SPACE => continue,
            character => {
                if character == limit {
                    return Ok(&content[i + 1..]);
                } else {
                    return Err(ParsingError::UnexpectedCharacter(character));
                }
            }
        }
    }

    Err(ParsingError::ReachedEnd)
}

struct Enumarated2SlidingWindow {
    chars: String,
    index: usize,
    head: Option<char>
}

impl Enumarated2SlidingWindow {
    fn from(content: &str) -> Self {
        let mut chars = content.chars().rev().collect::<String>();
        let head = chars.pop();
        Self {
            chars,
            head,
            index: 0
        }
    }
}


impl Iterator for Enumarated2SlidingWindow {
    type Item = (usize, char, Option<char>);
    fn next(&mut self) -> Option<Self::Item> {
        let head = self.head?;
        let popped = self.chars.pop();
        let res = (self.index, head, popped);

        self.head = popped;
        self.index += 1;
        Some(res)
    }
}

fn parse_string(content: &str) -> Result<(String, &str), ParsingError> {
    let mut name = String::new();
    let content = advance_until(content, QUOTATION)?;
    let mut skip = false;

    for (i, first, second) in Enumarated2SlidingWindow::from(content) {
        if skip {
            skip = false;
            continue
        }

        match first {
            REVERSE_SOLIDUS => {
                let second = second.ok_or(ParsingError::ReachedEnd)?;
                match second {
                    QUOTATION | REVERSE_SOLIDUS | '/' => name.push(second),
                    'b' => name.push(BACKSPACE),
                    'f' => name.push(FORMFEED),
                    'n' => name.push(LINE_FEED),
                    'r' => name.push(CARRIAGE_RETURN),
                    't' => name.push(CHARACTER_TABULATION),
                    _ => return Err(ParsingError::InvalidEscape(ContinuousError{begin: &content[i..], end: 1})),
                }
                skip = true
            },
            QUOTATION => return Ok((name, &content[i + 1..])),
            character => name.push(character),
        }
    }

    Err(ParsingError::ReachedEnd)
}

fn parse_array(content: &str) -> Result<(Vec<Value>, &str), ParsingError> {
    let mut result = vec![];
    let mut content = content;

    loop {
        let value;
        (value, content) = find_value(&content[1..])?;
        result.push(value);

        match next(content, R_SQ_BRACKET)? {
            Next::Over(val) => return Ok((result, val)),
            Next::NewPair(val) => {
                content = val;
            }
        }
    }
}

fn frac_from_string(string: &str) -> f64 {
    let mut result = 0.0;
    for (i, digit) in string.chars().enumerate() {
        let number: f64 = digit.to_digit(10).unwrap().into();
        result += number / 10_f64.powf((i + 1) as f64);
    }

    result
}

fn number_from_string(string: &str) -> f64 {
    let mut result = 0.0;
    for (i, digit) in string.chars().rev().enumerate() {
        let number: f64 = digit.to_digit(10).unwrap().into();
        result += number * 10_f64.powf(i as f64);
    }

    result
}

fn find_head(content: &str) -> Result<char, ParsingError> {
    Ok(content.chars().nth(0).ok_or(ParsingError::ReachedEnd)?)
}

fn parse_number(content: &str) -> Result<(f64, &str), ParsingError> {
    let begin_number = content;
    let mut error_offset = 0;
    let head = find_head(content)?;
    let (factor, content) = match head {
        MINUS => {
            error_offset += 1;
            (-1.0, &content[1..])
        },
        _ => (1.0, &content[..]),
    };

    let head = find_head(content)?;
    let (base, content) = match head {
        '0' => {
            error_offset += 1;
            (0.0, &content[1..])
        },
        '1'..='9' => {
            let mut base = String::new();
            let mut index = 0;
            for (i, c) in content.chars().enumerate() {
                index = i;

                if c.is_digit(10) {
                    base.push(c);
                } else {
                    break;
                }
            }
            error_offset += base.len();
            (number_from_string(&base), &content[index..])
        }
        character => return Err(ParsingError::UnexpectedCharacter(character)),
    };

    let head = find_head(content)?;
    let (frac, content) = match head {
        DECIMAL_POINT => {
            let mut frac = String::new();
            let mut index = 0;
            error_offset += 1;

            for (i, c) in content[1..].chars().enumerate() {
                index = i;

                if c.is_digit(10) {
                    frac.push(c)
                } else {
                    break;
                }
            }

            if frac.is_empty() {
                return Err(ParsingError::DecimalWithoutFrac(ContinuousError{begin: begin_number, end: error_offset}));
            }

            (Some(frac_from_string(&frac)), &content[index + 1..])
        }
        _ => (None, content),
    };

    let head = find_head(content)?;
    let (exponent, content) = match head {
        EXPONENT_LOWER | EXPONENT_UPPER => {
            let content = &content[1..];
            let head = find_head(content)?;

            let (factor, content) = match head {
                PLUS => (1.0, &content[1..]),
                MINUS => (-1.0, &content[1..]),
                _ => (1.0, content),
            };

            let mut base = String::new();
            let mut index = 0;
            for (i, c) in content.chars().enumerate() {
                index = i;

                if c.is_digit(10) {
                    base.push(c);
                } else {
                    break;
                }
            }

            (
                10_f64.powf(factor * number_from_string(&base)),
                &content[index..],
            )
        }
        _ => (1.0, content),
    };

    Ok((factor * (base + frac.unwrap_or(0.0)) * exponent, content))
}

fn parse_null(content: &str) -> Result<Value, ParsingError> {
    for (i, c) in content.chars().enumerate() {
        if c != NULL_LIT[i] {
            return Err(ParsingError::UnexpectedCharacter(c));
        }

        if i == NULL_LIT.len() - 1 {
            return Ok(Value::Null);
        }
    }

    Err(ParsingError::ReachedEnd)
}

fn parse_true(content: &str) -> Result<Value, ParsingError> {
    for (i, c) in content.chars().enumerate() {
        if c != TRUE_LIT[i] {
            return Err(ParsingError::UnexpectedCharacter(c));
        }

        if i == TRUE_LIT.len() - 1 {
            return Ok(Value::True);
        }
    }

    Err(ParsingError::ReachedEnd)
}

fn parse_false(content: &str) -> Result<Value, ParsingError> {
    for (i, c) in content.chars().enumerate() {
        if c != FALSE_LIT[i] {
            return Err(ParsingError::UnexpectedCharacter(c));
        }

        if i == FALSE_LIT.len() - 1 {
            return Ok(Value::False);
        }
    }

    Err(ParsingError::ReachedEnd)
}

fn find_value(content: &str) -> Result<(Value, &str), ParsingError> {
    const FIRST_NULL: char = NULL_LIT[0];
    const FIRST_TRUE: char = TRUE_LIT[0];
    const FIRST_FALSE: char = FALSE_LIT[0];

    for (i, c) in content.chars().enumerate() {
        match c {
            CHARACTER_TABULATION | LINE_FEED | CARRIAGE_RETURN | SPACE => continue,
            FIRST_NULL => return Ok((parse_null(&content[i..])?, &content[i + NULL_LIT.len()..])),
            FIRST_TRUE => return Ok((parse_true(&content[i..])?, &content[i + TRUE_LIT.len()..])),
            FIRST_FALSE => {
                return Ok((parse_false(&content[i..])?, &content[i + FALSE_LIT.len()..]))
            }
            L_CU_BRACKET => {
                let (object, rest) = parse_object(&content[i..])?;
                return Ok((Value::Obj(object), rest));
            }
            QUOTATION => {
                let (string, rest) = parse_string(&content[i..])?;
                return Ok((Value::String(string), rest));
            }
            L_SQ_BRACKET => {
                let (array, rest) = parse_array(&content[i..])?;
                return Ok((Value::Array(array), rest));
            }
            _ => {
                let (number, rest) = parse_number(&content[i..])?;
                return Ok((Value::Number(number), rest));
            }
        }
    }

    Err(ParsingError::ReachedEnd)
}

fn next(content: &str, end: char) -> Result<Next, ParsingError> {
    for (i, c) in content.chars().enumerate() {
        match c {
            CHARACTER_TABULATION | LINE_FEED | CARRIAGE_RETURN | SPACE => continue,
            COMMA => return Ok(Next::NewPair(&content[i + 1..])),
            character => {
                if character == end {
                    return Ok(Next::Over(&content[i + 1..]));
                } else {
                    return Err(ParsingError::UnexpectedCharacter(character));
                }
            }
        };
    }

    Err(ParsingError::ReachedEnd)
}

pub fn parse_object(content: &str) -> Result<(Object, &str), ParsingError> {
    let mut pairs = vec![];
    let mut content = advance_until(content, L_CU_BRACKET)?;

    loop {
        let (name, value);
        (name, content) = parse_string(content)?;
        content = advance_until(content, COLON)?;
        (value, content) = find_value(content)?;

        pairs.push((name, value));

        match next(content, R_CU_BRACKET)? {
            Next::Over(val) => return Ok((Object(pairs), val)),
            Next::NewPair(val) => {
                content = val;
            }
        };
    }
}
