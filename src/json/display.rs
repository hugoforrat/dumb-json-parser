use std::collections::VecDeque;
use std::fmt;

use super::parsing::Value;
use super::{Object, ParsingError, ContinuousError};

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.display_with_offset(""))
    }
}

struct Color(u8, u8, u8);
const RED: Color = Color(255, 0, 0);

fn color(string: &str, color: Color) -> String {
    format!("\x1b[38;2;{};{};{}m{}\x1b[0m",
             color.0, color.1, color.2, string)
}

enum DisplayResult {
    String(String),
    Strings(VecDeque<String>),
}

use DisplayResult::String as DRString;

trait CountedTrim {
    fn trim(&self) -> (&Self, usize, usize);
}

fn cnt_whitespace(state: (bool, usize), character: char) -> (bool, usize) {
    let (at_beginning, cnt) = state;

    if at_beginning && character.is_whitespace() {
        (at_beginning, cnt + 1)
    } else {
        (false, cnt)
    }
}

impl CountedTrim for str {
    fn trim(&self) -> (&str, usize, usize) {
        let (_, begin) = self.chars().fold((true, 0), cnt_whitespace);
        let (_, end) = self.chars().rev().fold((true, 0), cnt_whitespace);

        (self.trim(), begin, end)
    }
}

impl Object {
    fn display_with_offset(&self, offset: &str) -> String {
        let mut result = String::new();

        let max_len = self
            .0
            .iter()
            .fold(0, |acc, (x, _)| if x.len() > acc { x.len() } else { acc });

        let indent = format!("\t{}{:>max_len$}", offset, "");

        for (name, value) in self.0.iter() {
            let value = value.display_with_offset(&indent);
            match value {
                DRString(value) => {
                    result.push_str(&format!("{}{:>max_len$} |{}", offset, name, value))
                }
                DisplayResult::Strings(mut lines) => {
                    let first = lines.pop_front().unwrap();
                    result.push_str(&format!("{}{:>max_len$} |{}", offset, name, first));
                    for line in lines {
                        result.push_str(&format!("{}{:>max_len$}  {}", offset, "", line));
                    }
                }
            }
        }

        result
    }
}

impl Value {
    fn display_with_offset(&self, offset: &str) -> DisplayResult {
        return match self {
            Self::Obj(object) => DRString(format!("\n{}", object.display_with_offset(offset))),
            Self::Array(array) => {
                let mut s = String::from(" [\n");
                for value in array.iter().map(|val| val.display_with_offset("")) {
                    match value {
                        DRString(value) => s.push_str(&format!("{}\t{}", offset, value)),
                        DisplayResult::Strings(lines) => lines
                            .iter()
                            .map(|value| s.push_str(&format!("{}\t{}", offset, value)))
                            .collect(),
                    }
                }
                s.push_str(&format!("{}]\n", offset));
                DRString(s)
            }
            Self::Number(number) => DRString(format!(" {}\n", number)),
            Self::String(s) => {
                if !s.contains("\n") {
                    return DRString(format!(" \"{}\"\n", s));
                }

                let s = format!(" \"{}\"\n", s);
                DisplayResult::Strings(
                    s.lines()
                        .map(|line| format!("{}\n", line))
                        .collect::<VecDeque<String>>(),
                )
            }
            Self::True => DRString(format!(" True\n")),
            Self::False => DRString(format!(" False\n")),
            Self::Null => DRString(format!(" Null\n")),
        };
    }
}

fn find_line<'a>(content: &'a str, error: &ContinuousError) -> (&'a str, usize) {
    let index = content.rfind(error.begin).unwrap();
    let beginline = content[..index].rfind("\n").map_or(0, |x| x + 1);

    let line = if let Some(endline) = content[index..].find("\n") {
        &content[beginline..index+endline]
    } else {
        &content[beginline..]
    };

    let (line, begin, _) = CountedTrim::trim(line);
    let index = index - beginline - begin;

    (line, index)
}

fn show_continuous_error(error_msg: &str, content: &str, error: ContinuousError) {
    let (line, index) = find_line(content, &error);
    let show = line.chars().enumerate().map(|(i, _)| if (index..=index+error.end).contains(&i) {'^'} else {' '}).collect::<String>();
    let show = color(&show, RED);

    println!("{}", color(error_msg, RED));
    println!("{} {}", color(">", RED), line);
    println!("{} {}", color(">", RED), show);
}

impl ParsingError<'_> {
    pub fn show(self, content: &str) {
        use ParsingError::*;

        match self {
            UnexpectedCharacter(c)    => println!("Unexpected character: {}", c),
            DecimalWithoutFrac(error) => show_continuous_error("Decimal without fraction.", content, error),
            ReachedEnd                => println!("Reached the end unexpectedly"),
            InvalidEscape(error)      => show_continuous_error("Invalid escape error.", content, error)
        }
    }
}
