// Structural tokens
pub const L_SQ_BRACKET: char = '\u{005B}';
pub const R_SQ_BRACKET: char = '\u{005D}';

pub const L_CU_BRACKET: char = '\u{007B}';
pub const R_CU_BRACKET: char = '\u{007D}';

pub const COLON: char = '\u{003A}';
pub const COMMA: char = '\u{002C}';

// Whitespace
pub const CHARACTER_TABULATION: char = '\u{0009}';
pub const LINE_FEED: char = '\u{000A}';
pub const CARRIAGE_RETURN: char = '\u{000D}';
pub const SPACE: char = '\u{0020}';

// String
pub const QUOTATION: char = '\u{0022}';
pub const REVERSE_SOLIDUS: char = '\u{005C}';

// Literals
pub const NULL_LIT: [char; 4] = ['\u{006E}', '\u{0075}', '\u{006C}', '\u{006C}'];
pub const TRUE_LIT: [char; 4] = ['\u{0074}', '\u{0072}', '\u{0075}', '\u{0065}'];
pub const FALSE_LIT: [char; 5] = ['\u{0066}', '\u{0061}', '\u{006C}', '\u{0073}', '\u{0065}'];

// Number
pub const DECIMAL_POINT: char = '\u{002E}';
pub const EXPONENT_LOWER: char = '\u{0065}';
pub const EXPONENT_UPPER: char = '\u{0045}';
pub const PLUS: char = '\u{002B}';
pub const MINUS: char = '\u{002D}';

// Escape code
pub const BACKSPACE: char = '\u{0008}';
pub const FORMFEED: char = '\u{000c}';
