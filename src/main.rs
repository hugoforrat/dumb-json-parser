use std::env;
use std::fs::File;
use std::io::Read;

mod json;

fn main() -> Result<(), std::io::Error> {
    let args: Vec<String> = env::args().collect();
    let args = &args[1..];

    for arg in args {
        let mut content = String::new();

        let _ = match File::open(arg)?.read_to_string(&mut content) {
            Ok(_) => (),
            Err(kind) => {
                println!("Error: {}\nIgnoring", kind);
                continue;
            }
        };

        if let Some(obj) = json::Object::from(&content[..]) {
            print!("{}", obj);
        }
    }

    Ok(())
}
