mod display;
mod literals;
mod parsing;

use parsing::*;

pub struct Object(Vec<(String, Value)>);

pub struct ContinuousError<'a> {
    begin: &'a str,
    end: usize
}

pub enum ParsingError<'a> {
    UnexpectedCharacter(char),
    DecimalWithoutFrac(ContinuousError<'a>),
    ReachedEnd,
    InvalidEscape(ContinuousError<'a>),
}

impl Object {
    pub fn from(content: &str) -> Option<Self> {
        match parse_object(content) {
            Ok((obj, _)) => Some(obj),
            Err(error) => {
                error.show(content);
                None
            }
        }
    }
}
